import "./App.css"
import { useState } from "react"
import {
    FormControl,
    FormUncontrolledControl,
} from "./components/FormControl.jsx"

function App() {
    const [count, setCount] = useState(0)
    const [user, setUser] = useState({
        name: "toto",
        age: 13,
    })

    const increment = () => {
        setCount((c) => c + 1)
        setCount((c) => c + 1)
        setCount((c) => c + 1)
        console.log("increment")
    }

    const handleAge = () => {
        // user.age++
        console.log(user)
        setUser({ ...user, age: user.age + 1 })
    }

    return (
        <>
            <h1>Bonjour les gens, au revoir</h1>
            <FormControl />
            <FormUncontrolledControl />
            <section>
                <h2>Compteur : {count} </h2>
                <button onClick={increment}>Incrementer</button>
            </section>
            <section>
                <h2>
                    User age : {user.name} à {user.age} ans{" "}
                </h2>
                <button onClick={handleAge}>Gagner une année</button>
            </section>
        </>
    )
}

export default App
