import { useState } from "react"

const FormUncontrolledControl = () => {
    const handleSubmit = (e) => {
        e.preventDefault()
        const form = e.target
        const formData = new FormData(form)
        console.log(formData)
    }

    console.log("render form uncontrolled")

    return (
        <form className="form-box" onSubmit={handleSubmit}>
            <p>Ouvre la console pour voir les logs</p>
            <input
                type="text"
                name="firstname"
                placeholder="form uncontrolled"
            />
            <button type="submit">Envoyer</button>
        </form>
    )
}

const FormControl = () => {
    const [name, setName] = useState("")

    const handleSubmit = (e) => {
        e.preventDefault()
    }
    console.log("render form control")

    return (
        <form className="form-box">
            <p>Ouvre la console pour voir les logs</p>
            <input
                type="text"
                name="lastname"
                placeholder="form control"
                value={name}
                onChange={(e) => setName(e.target.value)}
            />
            <button onSubmit={handleSubmit}>Envoyer</button>
        </form>
    )
}

export { FormControl, FormUncontrolledControl }
