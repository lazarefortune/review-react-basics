# Review React basics

L'objectif de ce mini TP est de revoir les bases de React et de Vite pour en maitriser les fondamentaux.

### Nous allons voir : 

#### Les hooks
- useState
- useEffect
- useCallback